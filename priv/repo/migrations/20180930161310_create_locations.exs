defmodule Blender.Repo.Migrations.CreateLocations do
  use Ecto.Migration

  def change do
    create table(:locations) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :geo,     :geometry, null: false

      timestamps()
    end

    create unique_index(:locations, [:user_id])
    create unique_index(:locations, [:geo])
    create constraint(:locations, :valid_geo, check: "ST_IsValid(geo)")
  end
end
