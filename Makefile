.PHONY: test clean

brunch:
	if [ ! -d "./assets/node_modules" ]; \
        then \
	    cd assets && npm install; \
	fi

compile:
	mix compile

deps:
	mix deps.get

db: postgis-install pg-start
	mix ecto.create ecto.migrate

setup: brunch deps db

live: setup
	iex -S mix phx.server

start: setup phx-start

stop:
	echo "not implemented"

pg-start:
	sh priv/scripts/pg_start

pg-stop:
	sh priv/scripts/pg_stop

phx-start:
	mix phx.server

clean:
	mix clean

test:
	mix test $$file

postgis-install:
	sh priv/scripts/postgis_install
