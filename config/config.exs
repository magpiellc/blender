# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :blender,
  ecto_repos: [Blender.Repo]

# Configures the endpoint
config :blender, BlenderWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "nNU9fnRVylmtgwfm6m4B3EaJJfdJu9zsb/3fa4MLERbKUhFnn7/ehkq9+sgnr5eZ",
  render_errors: [view: BlenderWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Blender.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
