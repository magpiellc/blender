defmodule Blender.Users.Photo do
  use Ecto.Schema
  import Ecto.Changeset
  alias Blender.Users.User

  schema "photos" do
    field :url, :string
    belongs_to :user, User, foreign_key: :user_id

    timestamps()
  end

  @doc false
  def changeset(photo, attrs) do
    photo
    |> cast(attrs, [:url, :user_id])
    |> validate_required([:url, :user_id])
  end
end
