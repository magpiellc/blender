defmodule Blender.Users.Location do
  use Ecto.Schema
  import Ecto.Changeset

  alias Blender.Users.User

  schema "locations" do
    belongs_to :user, User, foreign_key: :user_id
    field :geo, Geo.PostGIS.Geometry

    timestamps(default: Ecto.DateTime.utc)
  end

  @doc false
  def changeset(location, attrs) do
    location
    |> cast(attrs, [:user_id, :geo])
    |> validate_required([:user_id, :geo])
    |> unique_constraint(:user_id)
  end

end
