defmodule Blender.Users.Token do

  alias Blender.Users.User

  @max_age 86400

  def sign(%User{} = user) do
    Phoenix.Token.sign(salt(), salt(), user.id)
  end

  def verify(token) do
    Phoenix.Token.verify(salt(), salt(), token, max_age: @max_age)
  end

  defp salt() do
    props = Application.get_env(:blender, BlenderWeb.Endpoint)
    Keyword.get(props, :secret_key_base)
  end

end
