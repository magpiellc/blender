defmodule Blender.GeoJson do
  def point(lon, lat) do
    %{"type" => "Point", "coordinates" =>  [lon, lat]}
  end
end
