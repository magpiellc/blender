defmodule BlenderWeb.UserView do
  use BlenderWeb, :view

  alias BlenderWeb.UserView
  alias Blender.Users.Token

  def render("index.json", %{users: users}) do
    %{users: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{user: render_one(user, UserView, "user.json")}
  end

  def render("show_details.json", %{user: user}) do
    %{user: render_one(user, UserView, "user_details.json")}
  end

  def render("user.json", %{user: user}) do
    %{name: user.name,
      email: user.email}
  end

  def render("user_details.json", %{user: user}) do
    %{id: user.id,
      name: user.name,
      email: user.email,
      token: Token.sign(user)}
  end

end
