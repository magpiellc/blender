defmodule BlenderWeb.UserController do
  use BlenderWeb, :controller

  alias Blender.Users
  alias Blender.Users.User
  alias Comeonin.Bcrypt

  action_fallback BlenderWeb.FallbackController

  def index(conn, _params) do
    users = Users.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Users.create_user(user_params) do
      conn
      |> put_status(:created)
      |> render("show.json", user: user)
    end
  end

  def get(conn, %{"email" => email}) do
    user = Users.get_by!(email: email)
    render(conn, "show.json", user: user)
  end

  def id(conn, %{"id" => id}) do
    user = Users.get_user!(id)
    render(conn, "show_details.json", user: user)
  end

  def update(conn, %{"id" => id,
                     "user_changes" => user_params}) do
    user = Users.get_user!(id)
    with {:ok, %User{} = user} <- Users.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Users.get_user!(id)
    with {:ok, %User{}} <- Users.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end

  def login(conn, %{"email" => email, "password" => password}) do
     ret =
      with user = %User{} <- Users.get_by(email: email),
           true <- Bcrypt.checkpw(password, user.password_hash) do
        render conn, "show_details.json", user: user
      end
     case ret do
       nil -> send_login_failure(conn)
       false -> send_login_failure(conn)
       _ -> ret
     end
  end

  defp send_login_failure(conn) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "bad credentials"})
  end

  defp authenticate(conn) do
    if conn.assigns.user_id do
      conn
    else
      conn
      |> halt()
    end
  end
end
