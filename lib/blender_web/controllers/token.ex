alias Blender.Users.User

defmodule Token do
  def sign(%User{} = user) do
    Phoenix.Token.sign(secret_salt(), secret_salt(), user.id)
  end

  def verify(token) do
    Phoenix.Token.verify(secret_salt(), secret_salt(),
      token, max_age: 86400)
  end

  def secret_salt() do
    props = Application.get_env(:blender, BlenderWeb.Endpoint)
    Keyword.get(props, :secret_key_base)
  end
end
