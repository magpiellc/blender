defmodule Blender.Auth do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    case get_req_header(conn, "authorization") do
      [] ->
        conn
        |> put_status(:unauthorized)
        |> Phoenix.Controller.text("unauthorized")
        |> halt

      [authorization | _] ->
        token = String.replace(authorization, "Bearer ", "")
        case Token.verify(token) do
          {:error, _} ->
            conn
            |> put_status(:unauthorized)
            |> Phoenix.Controller.text("unauthorized")
            |> halt
          {:ok, id} ->
            assign(conn, :user_id, id)
        end
    end
  end
end
