defmodule BlenderWeb.Router do
  use BlenderWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_authenticated do
    plug :accepts, ["json"]
    plug Blender.Auth
  end

  scope "/api", BlenderWeb do
    pipe_through :api

    # get("/health/check", HealthController, :check)

    post "/user", UserController, :create
    get "/user", UserController, :get
    post "/login", UserController, :login
  end

  scope "/api", BlenderWeb do
    pipe_through :api_authenticated

    get "/user/:id", UserController, :id
    patch "/user/:id", UserController, :update
    delete "/user/:id", UserController, :delete
  end

end
