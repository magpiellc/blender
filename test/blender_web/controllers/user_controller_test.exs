defmodule BlenderWeb.UserControllerTest do
  use BlenderWeb.ConnCase
  use BlenderWeb.TestHelpers

  describe "create user : " do

    test "a new user can be created", %{conn: conn} do
      # given
      user_info = %{"user" =>
                     %{"email" => "jack@test.com",
                       "name" => "Jack Frost",
                       "password" => "123456"}}
      # when
      conn = post conn, "/api/user", user_info
      # then
      response = json_response(conn, 201)
      user = response["user"]
      refute Map.has_key?(user, "id")
      assert user["name"] == "Jack Frost"
      assert user["email"] == "jack@test.com"
    end

    test "fails when email is missing", %{conn: conn} do
      # given
      user_info = %{"user" =>
                     %{"name" => "Jack Frost",
                       "password" => "123456"}}
      # when
      conn = post conn, "/api/user", user_info
      # then
      response = json_response(conn, 422)
      errors = response["errors"]
      assert errors["email"] == ["can\'t be blank"]
    end

    test "fails when user name is missing", %{conn: conn} do
      # given
      user_info = %{"user" =>
                     %{"email" => "jack@test.com",
                       "password" => "123456"}}
      # when
      conn = post conn, "/api/user", user_info
      # then
      response = json_response(conn, 422)
      errors = response["errors"]
      assert errors["name"] == ["can\'t be blank"]
    end

    test "fails when password is missing", %{conn: conn} do
      # given
      user_info = %{"user" =>
                     %{"email" => "jack@test.com",
                       "name" => "Jack Frost"}}
      # when
      conn = post conn, "/api/user", user_info
      # then
      response = json_response(conn, 422)
      errors = response["errors"]
      assert errors["password"] == ["can\'t be blank"]
    end

    test "fails when password length is less than 6", %{conn: conn} do
      # given
      user_info = %{"user" =>
                     %{"email" => "jack@test.com",
                       "name" => "Jack Frost",
                       "password" => "12345"}}
      # when
      conn = post conn, "/api/user", user_info
      # then
      response = json_response(conn, 422)
      errors = response["errors"]
      assert errors["password"] == ["should be at least 6 character(s)"]
    end

    test "fails when the email already exists", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      # when
      user_info = %{"user" =>
                     %{"email" => "jack@test.com",
                       "name" => "Jack Snow",
                       "password" => "abcdef"}}
      conn = post conn, "/api/user", user_info
      # then
      response = json_response(conn, 422)
      errors = response["errors"]
      assert errors["email"] == ["has already been taken"]
    end

  end

  describe "get user : " do

    test "succeeds when user with email exists", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      # when
      conn = get conn, "/api/user", email: "jack@test.com"
      # then
      response = json_response(conn, 200)
      user = response["user"]
      refute Map.has_key?(user, "id")
      refute Map.has_key?(user, "password")
      assert user["name"] == "Jack Frost"
      assert user["email"] == "jack@test.com"
    end

    test "fails with non-existent email", %{conn: conn} do
      #given
      #empty list of users
      #when
      assert_error_sent 404, fn ->
        get conn, "/api/user", email: "jack@test.com"
      end
    end

  end

  describe "update user : " do

    test "fails without proper authentication", %{conn: conn} do
      # given
      # when
      # then
      conn = patch conn, "/api/user/1", %{}
      assert response(conn, 401) == "unauthorized"
    end

    test "password can be updated", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      user_sess1 = user_logged_in(conn, "jack@test.com", "123456")
      # when
      user_info = %{"user_changes" =>
                     %{"password" => "new_password"}}
      token = user_sess1["token"]
      id = user_sess1["id"]
      response =
        conn
        |> put_req_header("authorization", auth_header_value(token))
        |> patch("/api/user/#{id}", user_info)
        |> json_response(200)
      # then
      user = response["user"]
      refute Map.has_key?(user, "id")
      refute Map.has_key?(user, "password")
      assert user["name"] == "Jack Frost"
      assert user["email"] == "jack@test.com"

      user_sess2 = user_logged_in(conn, "jack@test.com", "new_password")
      assert user_sess1["id"] == user_sess2["id"]
    end

    test "fails when password length is less than 6", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      user_sess = user_logged_in(conn, "jack@test.com", "123456")
      # when
      user_info = %{"user_changes" =>
                      %{"password" => "short"}}
      token = user_sess["token"]
      id = user_sess["id"]
      response =
        conn
        |> put_req_header("authorization", auth_header_value(token))
        |> patch("/api/user/#{id}", user_info)
        |> json_response(422)
      # then
      errors = response["errors"]
      assert errors["password"] == ["should be at least 6 character(s)"]
    end

    test "only updates password field", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      user_sess1 = user_logged_in(conn, "jack@test.com", "123456")
      # when
      user_info = %{"user_changes" =>
                     %{"name" => "Jack Snow",
                       "email" => "jack2@test.com",
                       "password" => "new_password"}}
      token = user_sess1["token"]
      id = user_sess1["id"]
      response =
        conn
        |> put_req_header("authorization", auth_header_value(token))
        |> patch("/api/user/#{id}", user_info)
        |> json_response(200)
      # then
      user = response["user"]
      refute Map.has_key?(user, "id")
      refute Map.has_key?(user, "password")
      assert user["name"] == "Jack Frost"
      assert user["email"] == "jack@test.com"

      user_sess2 = user_logged_in(conn, "jack@test.com", "new_password")
      assert user_sess1["id"] == user_sess2["id"]
    end

  end

  describe "delete user : " do

    test "fails without proper authentication", %{conn: conn} do
      # given
      # when
      # then
      conn = delete conn, "/api/user/1", %{}
      assert response(conn, 401) == "unauthorized"
    end

    test "succeeds when user is authenticated", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      user_sess1 = user_logged_in(conn, "jack@test.com", "123456")
      # when
      token = user_sess1["token"]
      id = user_sess1["id"]
      conn
      |> put_req_header("authorization", auth_header_value(token))
      |> delete("/api/user/#{id}", %{})
      |> response(204)
      # then
      assert_error_sent 404, fn ->
        get conn, "/api/user", email: "jack@test.com"
      end
    end

  end

  describe "login : " do

    test "succeeds with valid credentials", %{conn: conn} do
      #given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      # when
      login_info = %{"email" => "jack@test.com",
                     "password" => "123456"}
      conn = post conn, "/api/login", login_info
      # then
      response = json_response(conn, 200)
      user = response["user"]
      refute Map.has_key?(user, "password")
      assert Map.has_key?(user, "id")
      assert Map.has_key?(user, "token")
      assert user["name"] == "Jack Frost"
      assert user["email"] == "jack@test.com"
    end

    test "fails with incorrect password", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      # when
      login_info = %{"email" => "jack@test.com",
                     "password" => "123457"}
      conn = post conn, "/api/login", login_info
      # then
      response = json_response(conn, 401)
      assert response["error"] == "bad credentials"
    end

    test "fails with incorrect username", %{conn: conn} do
      # given
      user_created(conn, "Jack Frost", "jack@test.com", "123456")
      # when
      login_info = %{"email" => "jack1@test.com",
                     "password" => "123456"}
      conn = post conn, "/api/login", login_info
      # then
      response = json_response(conn, 401)
      assert response["error"] == "bad credentials"
    end

    test "fails with non existant user", %{conn: conn} do
      # given
      # no user exists in the db
      # when
      login_info = %{"email" => "jack@test.com",
                     "password" => "123456"}
      conn = post conn, "/api/login", login_info
      # then
      response = json_response(conn, 401)
      assert response["error"] == "bad credentials"
    end

  end
end
