defmodule Blender.Large.LocationTest do
  use BlenderWeb.ConnCase
  use BlenderWeb.TestHelpers
  alias Blender.Repo

  test "location/ping: authenticated user can update his location",
    %{conn: conn} do
    # given
    {:ok, user} = user_inserted_in_db("Jack Frost", "jack@test.com", "123456")
    token = Token.sign(user)
    loc = %{"geo" => %{"type" => "Point", "coordinates" => [100.0, 0.0]}}
    # when
    conn = TestClient.location_ping(conn, token, loc)
    # then
    response = json_response(conn, 200)
    assert response["status"] == "ok"
  end

  test "location/ping: fails without proper authentication token",
    %{conn: conn} do
    # given
    # no user in the db
    # when
    loc = %{"geo" => %{"type" => "Point",
                       "coordinates" => [100.0, 0.0]}}
    conn = TestClient.location_ping(conn, "bogus", loc)
    # then
    response = json_response(conn, 200)
    assert response["status"] == "error"
    assert response["error"]["code"] == "AUTH_INVALID"
  end

  test "location/ping: updates the recorded location for user",
    %{conn: conn} do
    # given
    {:ok, user} = user_inserted_in_db("Jack Frost", "jack@test.com", "123456")
    token = Token.sign(user)
    loc1 = %{"geo" => %{"type" => "Point", "coordinates" => [100.0, 0.0]}}
    loc2 = %{"geo" => %{"type" => "Point", "coordinates" => [120.0, 0.0]}}
    # when
    conn = TestClient.location_ping(conn, token, loc1)
    _ = TestClient.location_ping(conn, token, loc2)
    # then
    latest_location = Repo.all(Location, user_id: user.id) |> List.first
    assert latest_location.geo.coordinates == {120.0, 0.0}
  end

  test "location/nearby: returns users with the specified distance",
    %{conn: conn} do
    # given
    {:ok, user1} = user_inserted_in_db("Jack Frost", "jack@test.com", "123456")
    {:ok, user2} = user_inserted_in_db("Bugs bunny", "bugs@test.com", "654321")
    {:ok, user3} = user_inserted_in_db("Daffy Duck", "daffy@test.com", "a1b2c3")

    user_location_inserted_in_db(user1.id,
      [-118.4771113, 34.0350219]) # point 1
    user_location_inserted_in_db(user2.id,
      [-118.47739718, 34.03584718]) # this is with 200 meters of point1
    user_location_inserted_in_db(user3.id,
      [-118.47531979, 34.03478727]) # this is with 200 meters of point1

    # when
    params = %{
      "geo" => geo_point(-118.4771113, 34.0350219),
      "opts" => %{
        "distance" => 200 # meters
      }
    }
    conn = TestClient.location_nearby(conn, Token.sign(user1), params)
    # then
    response = json_response(conn, 200)
    assert response["status"] == "ok"
    assert length(response["users"]) == 2 # found within 200 meters
  end

  defp geo_point lon, lat do
    %{"type" => "Point",
      "coordinates" => [lon, lat]}
  end

end
