defmodule Blender.Large.HealthCheckTest do
  use BlenderWeb.ConnCase

  test "health/check: responds with ok when service is up",
                             %{conn: conn} do
    # given
    # service is up
    # when
    conn = TestClient.health_check(conn)
    # then
    response = json_response(conn, 200)
    assert response["status"] == "ok"
  end

end
