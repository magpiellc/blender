defmodule BlenderWeb.TestHelpers do
  use ExUnit.CaseTemplate

  alias Blender.Repo
  alias Blender.Users.{User, Photo}

  using do
    quote do
      defp user_created(conn, name, email, password) do
        user_info = %{"user" =>
                       %{"email" => email,
                         "name" => name,
                         "password" => password}}
        conn = post conn, "/api/user", user_info
        response = json_response(conn, 201)
        response["user"]
      end

      defp user_logged_in(conn, email, password) do
        login_info = %{"email" => email,
                       "password" => password}
        conn = post conn, "/api/login", login_info
        response = json_response(conn, 200)
        response["user"]
      end

      defp user_inserted_in_db name, email, password do
        {:ok, user} = Repo.insert(%User{email: email,
                                        name: name,
                                        password: password})
      end

      defp user_location_inserted_in_db user_id, loc do
        Location.upsert_location(user_id,
          %{"geo" =>
            %{"type" => "Point",
              "coordinates" => loc}})
      end

      defp user_photo_inserted_in_db user_id, url, pos do
        Photo.changeset(%Photo{}, %{
              :user_id => user_id,
              :url => url,
              :pos => pos}) |> Blender.Repo.insert()
      end

      defp auth_header_value token do
        "Bearer" <> " " <> token
      end

    end
  end

end
