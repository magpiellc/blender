defmodule BlenderWeb.TestClient do
  use Phoenix.ConnTest
  @endpoint BlenderWeb.Endpoint

  def health_check(conn) do
    conn |> http_get("/api/health/check", %{})
  end

  def user_create(conn, params) do
    conn |> http_post("/user/create", params)
  end

  def user_get(conn, params) do
    conn |> http_get("/user", params)
  end

  def user_login(conn, params) do
    conn |> http_post("/api/user/login", params)
  end

  def user_details(conn, token) do
    conn |> http_post("/api/user/details", token)
  end

  def user_details(conn, token, user_id) do
    conn |> http_post("/api/user/details", token, %{"user_id" => user_id})
  end

  def location_ping(conn, token, params) do
    conn |> http_post("/api/location/ping", token, params)
  end

  def location_nearby(conn, token, params) do
    conn |> http_post("/api/location/nearby", token, params)
  end

  def photo_request(conn, token, params) do
    conn |> http_post("/api/photo/request", token, params)
  end

  def photo_associate(conn, token, params) do
    conn |> http_post("/api/photo/associate", token, params)
  end

  def photo_rearrange(conn, token, params) do
    conn |> http_post("/api/photo/rearrange", token, params)
  end

  # public endpoint (no auth header) with request body
  def http_post(conn, endpoint, params) when is_map(params) do
    conn
    |> recycle
    |> post(endpoint, params)
  end

  # authenticated endpoint with no request body
  def http_post(conn, endpoint, token) do
    conn
    |> recycle
    |> auth(token)
    |> post(endpoint)
  end

  # authenticated endpoint with request body
  def http_post(conn, endpoint, token, params) do
    conn
    |> recycle
    |> auth(token)
    |> post(endpoint, params)
  end

  def http_get(conn, endpoint, params) when is_map(params) do
    conn
    |> recycle
    |> get(endpoint, params)
  end

  def auth(conn, token) do
    conn
    |> put_req_header("authorization", "Bearer #{token}")
  end
end
