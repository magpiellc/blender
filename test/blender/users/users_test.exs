defmodule Blender.UsersTest do
  use Blender.DataCase

  alias Blender.Users

  describe "users" do
    alias Blender.Users.User

    @valid_attrs %{email: "some email", name: "some name", password: "abcdef"}
    @update_attrs %{password: "new_password"}
    @invalid_attrs %{email: nil, name: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_user()

      Users.get_user!(user.id)
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Users.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Users.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Users.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.name == "some name"
      assert user.password == "abcdef"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Users.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.password == "new_password"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, @invalid_attrs)
      assert user == Users.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Users.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Users.change_user(user)
    end
  end

  describe "photos" do
    alias Blender.Users.Photo

    @valid_attrs %{url: "some url"}
    @update_attrs %{url: "some updated url"}
    @invalid_attrs %{url: nil, user_id: nil}

    setup do
      user = user_fixture()
      [user: user]
    end

    def photo_fixture(attrs \\ %{}) do
      {:ok, photo} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_photo()

      photo
    end

    test "list_photos/0 returns all photos", context do
      attrs = %{user_id: context[:user].id}
      photo = photo_fixture(attrs)
      assert Users.list_photos() == [photo]
    end

    test "get_photo!/1 returns the photo with given id", context do
      attrs = %{user_id: context[:user].id}
      photo = photo_fixture(attrs)
      assert Users.get_photo!(photo.id) == photo
    end

    test "create_photo/1 with valid data creates a photo", context do
      attrs = %{url: "some url", user_id: context[:user].id}
      assert {:ok, %Photo{} = photo} = Users.create_photo(attrs)
      assert photo.url == "some url"
    end

    test "create_photo/1 with invalid data returns error changeset", _context do
      assert {:error, %Ecto.Changeset{}} = Users.create_photo(@invalid_attrs)
    end

    test "update_photo/2 with valid data updates the photo", context do
      attrs = %{url: "test", user_id: context[:user].id}
      photo = photo_fixture(attrs)
      assert {:ok, photo} = Users.update_photo(photo, @update_attrs)
      assert %Photo{} = photo
      assert photo.url == "some updated url"
    end

    test "update_photo/2 with invalid data returns error changeset", context do
      attrs = %{url: "test", user_id: context[:user].id}
      photo = photo_fixture(attrs)
      assert {:error, %Ecto.Changeset{}} = Users.update_photo(photo, @invalid_attrs)
      assert photo == Users.get_photo!(photo.id)
    end

    test "delete_photo/1 deletes the photo", context do
      attrs = %{url: "test", user_id: context[:user].id}
      photo = photo_fixture(attrs)
      assert {:ok, %Photo{}} = Users.delete_photo(photo)
      assert_raise Ecto.NoResultsError, fn -> Users.get_photo!(photo.id) end
    end

    test "change_photo/1 returns a photo changeset", context do
      attrs = %{url: "test", user_id: context[:user].id}
      photo = photo_fixture(attrs)
      assert %Ecto.Changeset{} = Users.change_photo(photo)
    end

    test "list_users/0 returns photos associated with user", context do
      user = context[:user]
      attrs = %{url: "test", user_id: user.id}
      photo1 = photo_fixture(attrs)
      photo2 = photo_fixture(attrs)

      [user] = Users.list_users()
      assert user.photos == [photo2, photo1]
    end

    test "get_user!/1 returns photos associated with user", context do
      user = context[:user]
      photo1 = photo_fixture(%{url: "photo1", user_id: user.id})
      photo2 = photo_fixture(%{url: "photo2", user_id: user.id})

      user = Users.get_user!(user.id)
      assert user.photos == [photo2, photo1]
    end

  end

  describe "locations" do
    alias Blender.Users.Location
    alias Blender.GeoJson

    @valid_attrs %{geo: GeoJson.point(10, 10)}
    @update_attrs %{geo: GeoJson.point(20, 10)}
    @invalid_attrs %{geo: nil}

    setup do
      user = user_fixture()
      [user: user]
    end

    def location_fixture(attrs \\ %{}) do
      {:ok, location} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Users.create_location()

      location
    end

    test "list_locations/0 returns all locations", context do
      attrs = %{user_id: context[:user].id}
      location = location_fixture(attrs)
      assert Users.list_locations() == [location]
    end

    test "get_location!/1 returns the location with given id", context do
      attrs = %{user_id: context[:user].id}
      location = location_fixture(attrs)
      assert Users.get_location!(location.id) == location
    end

    test "create_location/1 with valid data creates a location", context do
      loc = GeoJson.point(10,10)
      attrs = %{geo: loc, user_id: context[:user].id}
      assert {:ok, %Location{} = location} = Users.create_location(attrs)
      coord = location.geo.coordinates
      assert coord == List.to_tuple(loc["coordinates"])
    end

    test "create_location/1 with invalid data returns error changeset",
      _context do
      assert {:error, %Ecto.Changeset{}} = Users.create_location(@invalid_attrs)
    end

    test "update_location/2 with valid data updates the location", context do
      attrs = %{user_id: context[:user].id}
      location = location_fixture(attrs)
      assert {:ok, location} = Users.update_location(location, @update_attrs)
      assert %Location{} = location
      coord = location.geo.coordinates
      assert coord == {20, 10}
    end

    test "update_location/2 with invalid data returns error changeset", context do
      attrs = %{user_id: context[:user].id}
      location = location_fixture(attrs)
      assert {:error, %Ecto.Changeset{}} =
        Users.update_location(location, @invalid_attrs)
      assert location == Users.get_location!(location.id)
    end

    test "delete_location/1 deletes the location", context do
      attrs = %{user_id: context[:user].id}
      location = location_fixture(attrs)
      assert {:ok, %Location{}} = Users.delete_location(location)
      assert_raise Ecto.NoResultsError,
          fn -> Users.get_location!(location.id) end
    end

    test "change_location/1 returns a location changeset", context do
      attrs = %{user_id: context[:user].id}
      location = location_fixture(attrs)
      assert %Ecto.Changeset{} = Users.change_location(location)
    end

    test "list_users/0 returns location associated with user", context do
      loc = GeoJson.point(10,10)
      user = context[:user]
      attrs = %{geo: loc, user_id: user.id}
      location = location_fixture(attrs)
      [user1] = Users.list_users()
      assert user1.location == location
    end

    test "get_user!/1 returns location associated with user", context do
      loc = GeoJson.point(10,10)
      user = context[:user]
      attrs = %{geo: loc, user_id: user.id}
      location = location_fixture(attrs)
      user1 = Users.get_user!(user.id)
      assert user1.location == location
    end
  end
end
